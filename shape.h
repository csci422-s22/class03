#ifndef SHAPE_H_
#define SHAPE_H_

#include <cmath>
#include <string>

class Shape {

public:

  // shape is an abstract class because it has a purely virtual function
  virtual double area() const = 0;

  const std::string& name() const { return name_; }

protected:
  std::string name_;
};

class Rectangle : public Shape {
public:
  Rectangle( double length , double width ) :
    length_(length),
    width_(width) {

    name_ = "rectangle";
  }

  double area() const {
    return length_*width_;
  }
private:
  double length_;
  double width_;
};

class Circle : public Shape {
public:
  Circle( double radius ) :
    radius_(radius) {

    name_ = "circle";
  }

  double area() const {
    return M_PI * radius_ * radius_;
  }

private:
  double radius_;
};

#endif
