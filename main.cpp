#include "myvector.h"
#include "matrix.h"
#include "shape.h"

#include <iostream>
#include <memory>

template<typename T>
T
add( T x , T y ) {
  return x + y;
}

int
main( int argc, char** argv ) {

  // uncommenting the following creates a compiler error
  // because Shape is an abstract class since area() is a pure virtual function
  //Shape shape;

  Circle circle(2.0);
  std::cout << circle.name() << " has area " << circle.area() << std::endl;

  // example of using a unique_ptr
  std::unique_ptr<Rectangle> r_ptr = std::make_unique<Rectangle>(10,20);
  Rectangle& r = * r_ptr.get();

  std::cout << r.name() << " has area " << r.area() << std::endl;
  std::cout << r_ptr->name() << " has area " << r_ptr->area() << std::endl;

  int z_i = add<int>(1,2);
  double z_f = add<double>(1.5,2.25);

  std::cout << "z_i = " << z_i << " , " << "z_f = " << z_f << std::endl;

  //Vector<3> v;
  //v(0) = 2;
  //v.print();

  // exercise 1: create 2d vectors u and v, and write a function to add them together


  // exercise 2: complete the matrix class implementation

  return 0;

}
