#ifndef MATRIX_H_
#define MATRIX_H_

#include <iostream>
#include <cstdlib>

template<int M, int N, typename T>
class Matrix {

public:
  // initialize all elements to 0
  Matrix();

  // const access to entry in row i, column j to retrieve entry
  const T& operator() (int i , int j) const;

  // non-const access to entry in row i, column j to modify entry
  T& operator() (int i , int j);

  // print out the contents of this matrix
  void print() const;

private:
  // how will you store the data?
};

#endif
