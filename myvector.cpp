#include "myvector.h"

#include <cassert>
#include <iostream>

template<int N>
Vector<N>::Vector() {
  for (int i = 0; i < N; i++) {
    data_[i] = 0;
  }
}

template<int N>
double&
Vector<N>::operator() (int i) {
  assert( i >= 0 && i < N );
  return data_[i];
}

template<int N>
void
Vector<N>::print() const {
  for (int i = 0; i < N; i++)
    std::cout << "entry[" << i << "] = " << data_[i] << std::endl;
}

template<>
void
Vector<2>::print() const {
  std::cout << "vec2d = (" << data_[0] << "," << data_[1] << ")" << std::endl;
}

template<>
void
Vector<3>::print() const {
  std::cout << "vec3d = (" << data_[0] << "," << data_[1] <<  "," << data_[2] << ")" << std::endl;
}
